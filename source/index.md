---
title: Handbook
date: 
catalog: false
layout: home
---

# Descrição

Esta é uma página central de como as coisas funcionam na Snowman Labs. Como parte de nossos valores é ser transparente, esse Handbook é aberto para o mundo e convidamos a dar seu feedback sobre. Por favor, faça um merger request para sugerir melhorias e correções. Para dúvidas e problemas, por favor, abra uma issue.

* Geral
    * Valores
* Pessoas
    * Comunicação
    * Código de Conduta
    * Liderança
    * Contratação
    * Onboarding
* Desenvolvimento
    * Infraestrutura
    * Code Review
* Marketing
    * Design
        * UX Design
        * UI Design
* Vendas
* Financeiro
* Juridico
* Outros
